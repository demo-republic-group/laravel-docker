# laravel-docker: project for test use laravel in Docker (with HELM install)   
## Quick Installation:  
```bash
git clone https://gitlab.com/demo-republic-group/laravel-docker.git
helm install laravel-d laravel-docker/helm/laravel-d-1.0.0.tgz
```
Open your ingress URL with path /laravel, like this:  
**http://host:port/laravel**

## NOTES:  
Be sure image **registry.gitlab.com/demo-republic-group/laravel-docker:1.0.0** is available from your kubertnetes node.

