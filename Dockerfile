# docker build -t laravel-test-docker  .
FROM laravelsail/php73-composer:latest
WORKDIR /opt
COPY . ./
COPY ./.env.example ./.env
RUN composer update
RUN php artisan sail:install --with=mysql,redis,meilisearch,mailhog,selenium
RUN php artisan key:generate
CMD php artisan serve --host=0.0.0.0 --port=80


